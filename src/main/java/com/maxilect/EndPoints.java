package com.maxilect;

/**
 * Created by nestor on 10.02.2019.
 */
public class EndPoints {

    public static final String USERS = "/rs/users/";
    public static final String USERS_WITH_ID = "/rs/users/{userId}";
}
