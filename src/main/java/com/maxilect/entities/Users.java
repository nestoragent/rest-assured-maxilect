package com.maxilect.entities;

import com.google.gson.annotations.SerializedName;

/**
 * Created by nestor on 10.02.2019.
 */
public class Users {
    @SerializedName("ID")
    public int id;

    @SerializedName("FIRSTNAME")
    public String firstName;

    @SerializedName("LASTNAME")
    public String lastName;

    @Override
    public String toString() {
        return "Users{" +
                "id='" + id + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                '}';
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int result = 17;
        result = 31 * result + id;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this)
            return true;
        if (!(obj instanceof Users))
            return false;
        Users user = (Users) obj;
        if (firstName.equals(user.firstName)
                && lastName.equals(user.getLastName())
                && id == ((Users) obj).id)
            return true;
        return false;
    }
}
