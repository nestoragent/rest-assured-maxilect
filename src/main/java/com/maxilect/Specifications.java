package com.maxilect;

import com.jayway.restassured.builder.RequestSpecBuilder;
import com.jayway.restassured.builder.ResponseSpecBuilder;
import com.jayway.restassured.filter.log.LogDetail;
import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.specification.RequestSpecification;
import com.jayway.restassured.specification.ResponseSpecification;

/**
 * Created by nestor on 10.02.2019.
 */
public class Specifications {

    private static RequestSpecification requestSpec;
    private static ResponseSpecification responseSpec;

    public static RequestSpecification getRequestSpecification() {
        requestSpec = new RequestSpecBuilder()
                .setBaseUri("http://localhost")
                .setPort(28080)
//                .setAccept(ContentType.JSON)
                .setContentType(ContentType.TEXT)
                .log(LogDetail.ALL)
                .build();
        return requestSpec;
    }

    public static ResponseSpecification getResponseSpecification() {
        responseSpec = new ResponseSpecBuilder()
                .expectStatusCode(200)
                .build();
        return responseSpec;
    }

    public static ResponseSpecification getEmptyResponseSpecification() {
        responseSpec = new ResponseSpecBuilder()
                .build();
        return responseSpec;
    }
}
