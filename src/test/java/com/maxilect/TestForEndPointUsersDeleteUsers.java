package com.maxilect;


import com.jayway.restassured.RestAssured;
import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.response.Response;
import com.maxilect.entities.Users;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Title;
import net.thucydides.core.annotations.WithTag;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Arrays;
import java.util.concurrent.ThreadLocalRandom;

import static com.jayway.restassured.RestAssured.given;


/**
 * Created by nestor on 10.02.2019.
 */
@RunWith(SerenityRunner.class)
@WithTag(name = "DeleteUsers")
public class TestForEndPointUsersDeleteUsers {

    private Users preparedUser = null;

    @BeforeClass
    public static void prepareSpecs() {
        RestAssured.requestSpecification = Specifications.getRequestSpecification();
        RestAssured.responseSpecification = Specifications.getResponseSpecification();
    }

    public void prepareData() {
        Response response = given()
                .contentType(ContentType.JSON)
                .header("firstName", "PreparedUsersTestForEndPointUsersGetUsers"
                        + ThreadLocalRandom.current().nextInt(100, 100000))
                .header("lastName", "PreparedUsersTestForEndPointUsersGetUsers"
                        + ThreadLocalRandom.current().nextInt(100, 100000))
                .when()
                .post(EndPoints.USERS)
                .thenReturn();
        preparedUser = Arrays.asList(response.getBody().as(Users[].class)).get(0);
    }

    @Test
    @Title("Id: 12. Позитивный. Тест удаление существующего пользователя")
    public void test_positive_shouldDeleteExistUser() {
        prepareData();
        given()
                .contentType(ContentType.JSON)
                .pathParam("userId", preparedUser.getId())
                .when()
                .delete(EndPoints.USERS_WITH_ID)
                .thenReturn();
        Assert.assertEquals("[]",
                getUserFromAPI().getBody().asString()
                        .replaceAll("\n", "").replaceAll(" ", "").trim());
    }

    @Test
    @Title("Id: 13. Позитивный. Тест удаление несуществующего пользователя")
    public void test_positive_shouldDeleteNonExistUser() {
        Response response = given()
                .contentType(ContentType.JSON)
                .pathParam("userId", -1)
                .when()
                .delete(EndPoints.USERS_WITH_ID)
                .thenReturn();
        Assert.assertEquals("",
                response.getBody().asString()
                        .replaceAll("\n", "").replaceAll(" ", "").trim());
    }

    @Test
    @Title("Id: 14. Негативный. Тест удаление пользователя с id String")
    public void test_negative_shouldDeleteUserWithStringId() {
        RestAssured.responseSpecification = Specifications.getEmptyResponseSpecification();
        Response response = given()
                .contentType(ContentType.JSON)
                .pathParam("userId", "testId")
                .when()
                .delete(EndPoints.USERS_WITH_ID)
                .thenReturn();

        Assert.assertEquals("Response not as expected.", 500, response.getStatusCode());
        Assert.assertTrue(response.body().asString().contains("SQLSyntaxErrorException: Column 'TESTID'"));
    }

    private Response getUserFromAPI() {
        return given()
                .contentType(ContentType.JSON)
                .pathParam("userId", preparedUser.getId())
                .when()
                .get(EndPoints.USERS_WITH_ID)
                .thenReturn();
    }
}
