package com.maxilect;


import com.jayway.restassured.RestAssured;
import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.response.Response;
import com.maxilect.entities.Users;
import com.maxilect.helpers.UserAssertHelper;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Title;
import net.thucydides.core.annotations.WithTag;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import static com.jayway.restassured.RestAssured.given;


/**
 * Created by nestor on 10.02.2019.
 */
@RunWith(SerenityRunner.class)
@WithTag(name = "UpdateEntity")
public class TestForEndPointUsersUpdateEntity {

    private Users preparedUser = null;

    @BeforeClass
    public static void prepareSpecs() {
        RestAssured.requestSpecification = Specifications.getRequestSpecification();
        RestAssured.responseSpecification = Specifications.getResponseSpecification();
    }

    @Before
    public void prepareData() {
        Response response = given()
                .contentType(ContentType.JSON)
                .header("firstName", "PreparedUsersTestForEndPointUsersUpdateUsers"
                        + ThreadLocalRandom.current().nextInt(100, 100000))
                .header("lastName", "PreparedUsersTestForEndPointUsersUpdateUsers"
                        + ThreadLocalRandom.current().nextInt(100, 100000))
                .when()
                .post(EndPoints.USERS)
                .thenReturn();
        preparedUser = Arrays.asList(response.getBody().as(Users[].class)).get(0);
    }

    @Test
    @Title("Id: 8. Позитивный. Тест обновление данных пользователя")
    public void test_positive_shouldUpdateUserData() {
        preparedUser.setFirstName("NewUserFirstName" + ThreadLocalRandom.current().nextInt(100, 100000));
        preparedUser.setLastName("NewUserLastName" + ThreadLocalRandom.current().nextInt(100, 100000));
        given()
                .contentType(ContentType.JSON)
                .pathParam("userId", preparedUser.getId())
                .header("firstName", preparedUser.getFirstName())
                .header("lastName", preparedUser.getLastName())
                .when()
                .put(EndPoints.USERS_WITH_ID);

        List<Users> receivedUsers = Arrays.asList(getUserFromAPI().getBody().as(Users[].class));
        UserAssertHelper.assertCreatedUser(preparedUser, receivedUsers.get(0));
    }

    @Test
    @Title("Id: 9. Позитивный. Тест обновление данных пользователя с пустыми значения")
    public void test_positive_shouldStoreNewUserWithEmptyVariables() {
        preparedUser.setFirstName("");
        preparedUser.setLastName("");

        given()
                .contentType(ContentType.JSON)
                .pathParam("userId", preparedUser.getId())
                .header("firstName", preparedUser.getFirstName())
                .header("lastName", preparedUser.getLastName())
                .when()
                .put(EndPoints.USERS_WITH_ID);
        Assert.assertTrue(getUserFromAPI().body().asString().contains("FIRSTNAME=, LASTNAME="));
    }

    @Test
    @Title("Id: 10. Позитивный. Тест добавление пользователя с граничными значениями")
    public void test_positive_shouldStoreNewUserWith100lengthVariables() {
        preparedUser.setFirstName("1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890");
        preparedUser.setLastName("1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890");

        given()
                .contentType(ContentType.JSON)
                .pathParam("userId", preparedUser.getId())
                .header("firstName", preparedUser.getFirstName())
                .header("lastName", preparedUser.getLastName())
                .when()
                .put(EndPoints.USERS_WITH_ID);

        List<Users> receivedUsers = Arrays.asList(getUserFromAPI().getBody().as(Users[].class));
        UserAssertHelper.assertCreatedUser(preparedUser, receivedUsers.get(0));
    }

    @Test
    @Title("Id: 11. Негативный. Тест обновление пользователя с длинной данных > 100")
    public void test_negative_shouldStoreNewUserWithVariablesLengthMore100() {
        RestAssured.responseSpecification = Specifications.getEmptyResponseSpecification();
        preparedUser.setFirstName("12345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901");
        preparedUser.setLastName("12345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901");

        Response response = given()
                .contentType(ContentType.JSON)
                .pathParam("userId", preparedUser.getId())
                .header("firstName", preparedUser.getFirstName())
                .header("lastName", preparedUser.getLastName())
                .when()
                .put(EndPoints.USERS_WITH_ID)
                .thenReturn();
        Assert.assertEquals("Response not as expected.", 500, response.getStatusCode());
        Assert.assertTrue(response.body().asString().contains("SQLDataException: A truncation error was encountered trying to shrink VARCHAR"));
    }


    private Response getUserFromAPI() {
        return given()
                .contentType(ContentType.JSON)
                .pathParam("userId", preparedUser.getId())
                .when()
                .get(EndPoints.USERS_WITH_ID)
                .thenReturn();
    }
}
