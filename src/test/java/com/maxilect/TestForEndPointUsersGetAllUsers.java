package com.maxilect;


import com.jayway.restassured.RestAssured;
import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.response.Response;
import com.maxilect.entities.Users;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Title;
import net.thucydides.core.annotations.WithTag;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Arrays;
import java.util.concurrent.ThreadLocalRandom;

import static com.jayway.restassured.RestAssured.given;


/**
 * Created by nestor on 10.02.2019.
 */
@RunWith(SerenityRunner.class)
@WithTag(name = "GetAllUsers")
public class TestForEndPointUsersGetAllUsers {

    private Users preparedUser = null;

    @BeforeClass
    public static void prepareSpecs() {
        RestAssured.requestSpecification = Specifications.getRequestSpecification();
        RestAssured.responseSpecification = Specifications.getResponseSpecification();
    }

    @Before
    public void prepareData() {
        Response response = given()
                .contentType(ContentType.JSON)
                .header("firstName", "PreparedUsersTestForEndPointUsersGetAllUsers"
                        + ThreadLocalRandom.current().nextInt(100, 100000))
                .header("lastName", "PreparedUsersTestForEndPointUsersGetAllUsers"
                        + ThreadLocalRandom.current().nextInt(100, 100000))
                .when()
                .post(EndPoints.USERS)
                .thenReturn();
        preparedUser = Arrays.asList(response.getBody().as(Users[].class)).get(0);
    }

    @Test
    @Title("Id: 15. Позитивный. Тест запрос всех существующих пользователей")
    public void test_positive_shouldGetAllExistUsers() {
        Response response = given()
                .contentType(ContentType.JSON)
                .when()
                .get(EndPoints.USERS)
                .thenReturn();
        Assert.assertTrue(response.getBody().asString().contains(preparedUser.getId() + ""));
        Assert.assertTrue(response.getBody().asString().contains(preparedUser.getFirstName()));
        Assert.assertTrue(response.getBody().asString().contains(preparedUser.getLastName()));
    }
}
