package com.maxilect.helpers;

import com.maxilect.entities.Users;
import org.junit.Assert;

public class UserAssertHelper {

    public static void assertCreatedUser(Users expectedUser, Users receivedUsers) {
        Assert.assertEquals("Response not as expected.",
                expectedUser.getFirstName(), receivedUsers.getFirstName());
        Assert.assertEquals("Response not as expected.", expectedUser.getLastName(), receivedUsers.getLastName());
        Assert.assertNotEquals("Response not as expected.", 0, receivedUsers.getId());
    }
}
