package com.maxilect;


import com.jayway.restassured.RestAssured;
import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.response.Response;
import com.maxilect.entities.Users;
import com.maxilect.helpers.UserAssertHelper;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Title;
import net.thucydides.core.annotations.WithTag;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Arrays;
import java.util.List;

import static com.jayway.restassured.RestAssured.given;


/**
 * Created by nestor on 10.02.2019.
 */
@RunWith(SerenityRunner.class)
@WithTag(name = "CreateEntity")
public class TestForEndPointUsersCreateEntity {


    @BeforeClass
    public static void prepareSpecs() {
        RestAssured.requestSpecification = Specifications.getRequestSpecification();
        RestAssured.responseSpecification = Specifications.getResponseSpecification();
    }

    @Test
    @Title("Id: 1. Позитивный. Тест добавление пользователя")
    public void test_positive_shouldStoreNewUser() {
        Users expectedUser = new Users();
        expectedUser.setFirstName("FirstNameNew");
        expectedUser.setLastName("LastNameNew");

        Response response = given()
                .contentType(ContentType.JSON)
                .header("firstName", expectedUser.getFirstName())
                .header("lastName", expectedUser.getLastName())
                .when()
                .post(EndPoints.USERS)
                .thenReturn();
        List<Users> receivedUsers = Arrays.asList(response.getBody().as(Users[].class));

        UserAssertHelper.assertCreatedUser(expectedUser, receivedUsers.get(0));
    }

    @Test
    @Title("Id: 2. Позитивный. Тест добавление пользователя с пустыми значения")
    public void test_positive_shouldStoreNewUserWithEmptyVariables() {
        Users expectedUser = new Users();
        expectedUser.setFirstName("");
        expectedUser.setLastName("");

        Response response = given()
                .contentType(ContentType.JSON)
                .header("firstName", expectedUser.getFirstName())
                .header("lastName", expectedUser.getLastName())
                .when()
                .post(EndPoints.USERS)
                .thenReturn();
        Assert.assertTrue(response.body().asString().contains("FIRSTNAME=, LASTNAME="));
    }

    @Test
    @Title("Id: 3. Позитивный. Тест добавление пользователя с граничными значениями")
    public void test_positive_shouldStoreNewUserWith100lengthVariables() {
        Users expectedUser = new Users();
        expectedUser.setFirstName("1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890");
        expectedUser.setLastName("1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890");

        Response response = given()
                .contentType(ContentType.JSON)
                .header("firstName", expectedUser.getFirstName())
                .header("lastName", expectedUser.getLastName())
                .when()
                .post(EndPoints.USERS)
                .thenReturn();
        response.prettyPrint();

        List<Users> receivedUsers = Arrays.asList(response.getBody().as(Users[].class));

        UserAssertHelper.assertCreatedUser(expectedUser, receivedUsers.get(0));
    }

    @Test
    @Title("Id: 4. Негативный. Тест добавление пользователя с длинной значений > 100")
    public void test_negative_shouldStoreNewUserWithVariablesLengthMore100() {
        RestAssured.responseSpecification = Specifications.getEmptyResponseSpecification();
        Users expectedUser = new Users();
        expectedUser.setFirstName("12345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901");
        expectedUser.setLastName("12345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901");

        Response response = given()
                .contentType(ContentType.JSON)
                .header("firstName", expectedUser.getFirstName())
                .header("lastName", expectedUser.getLastName())
                .when()
                .post(EndPoints.USERS)
                .thenReturn();
        Assert.assertEquals("Response not as expected.", 500, response.getStatusCode());
        Assert.assertTrue(response.body().asString().contains("SQLDataException: A truncation error was encountered trying to shrink VARCHAR"));
    }
}
