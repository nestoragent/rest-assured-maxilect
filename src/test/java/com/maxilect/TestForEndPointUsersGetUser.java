package com.maxilect;


import com.jayway.restassured.RestAssured;
import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.response.Response;
import com.maxilect.entities.Users;
import com.maxilect.helpers.UserAssertHelper;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Title;
import net.thucydides.core.annotations.WithTag;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import static com.jayway.restassured.RestAssured.given;


/**
 * Created by nestor on 10.02.2019.
 */
@RunWith(SerenityRunner.class)
@WithTag(name = "GetUser")
public class TestForEndPointUsersGetUser {

    private Users preparedUser = null;

    @BeforeClass
    public static void prepareSpecs() {
        RestAssured.requestSpecification = Specifications.getRequestSpecification();
        RestAssured.responseSpecification = Specifications.getResponseSpecification();
    }

    public void prepareData() {
        Response response = given()
                .contentType(ContentType.JSON)
                .header("firstName", "PreparedUsersTestForEndPointUsersGetUsers"
                        + ThreadLocalRandom.current().nextInt(100, 100000))
                .header("lastName", "PreparedUsersTestForEndPointUsersGetUsers"
                        + ThreadLocalRandom.current().nextInt(100, 100000))
                .when()
                .post(EndPoints.USERS)
                .thenReturn();
        preparedUser = Arrays.asList(response.getBody().as(Users[].class)).get(0);
    }

    @Test
    @Title("Id: 5. Позитивный. Тест запрос существующего пользователя")
    public void test_positive_shouldGetExistUser() {
        prepareData();
        Response response = given()
                .contentType(ContentType.JSON)
                .pathParam("userId", preparedUser.getId())
                .when()
                .get(EndPoints.USERS_WITH_ID)
                .thenReturn();
        List<Users> receivedUsers = Arrays.asList(response.getBody().as(Users[].class));
        UserAssertHelper.assertCreatedUser(preparedUser, receivedUsers.get(0));
    }

    @Test
    @Title("Id: 6. Позитивный. Тест запрос несуществующего пользователя")
    public void test_positive_shouldGetNonExistUser() {
        Response response = given()
                .contentType(ContentType.JSON)
                .pathParam("userId", -1)
                .when()
                .get(EndPoints.USERS_WITH_ID)
                .thenReturn();

        Assert.assertEquals("[]",
                response.getBody().asString()
                        .replaceAll("\n", "").replaceAll(" ", "").trim());
    }

    @Test
    @Title("Id: 7. Негативный. Тест запрос пользователя с id String")
    public void test_negative_shouldGetUserWithStringId() {
        RestAssured.responseSpecification = Specifications.getEmptyResponseSpecification();
        Response response = given()
                .contentType(ContentType.JSON)
                .pathParam("userId", "testId")
                .when()
                .get(EndPoints.USERS_WITH_ID)
                .thenReturn();

        Assert.assertEquals("Response not as expected.", 500, response.getStatusCode());
        Assert.assertTrue(response.body().asString().contains("SQLSyntaxErrorException: Column 'TESTID'"));
    }
}
